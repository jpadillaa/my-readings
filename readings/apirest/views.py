import os
from datetime import timedelta
from flask import request, current_app, send_from_directory
from werkzeug.utils import secure_filename
from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity, JWTManager
from flask_restful import Api

from apirest import api, db
from apirest.models import Usuario, Libro, usuario_schema, libro_schema, libros_schema

'''
Recurso que administra el servicio de login
'''
class RecursoLogin(Resource):
    def post(self):
        request.get_json(force=True)
        usuario = Usuario.query.get(request.json['email'])
        
        if usuario is None:
            return {'message':'El email ingresado no está registrado'}, 400
        
        if not usuario.verificar_clave(request.json['password']):
            return {'message': 'Contraseña incorrecta'}, 400
        
        try:
            access_token = create_access_token(identity = request.json['email'], expires_delta = timedelta(days = 1))
            return {
                'message':'Sesion iniciada',
                'access_token':access_token
            }
        
        except:
            return {'message':'Ha ocurrido un error'}, 500
    
'''
Recurso que administra el servicio de registro
'''
class RecursoRegistro(Resource):
    def post(self):
        if Usuario.query.filter_by(email=request.json['email']).first() is not None:
            return {'message': f'El correo({request.json["email"]}) ya está registrado'}, 400
        
        if request.json['email'] == '' or request.json['password'] == '' or request.json['usuario'] == '':
            return {'message': 'Campos invalidos'}, 400
        
        nuevo_usuario = Usuario(
            email = request.json['email'],
            password = request.json['password'],
            usuario = request.json['usuario'],
        )
        
        nuevo_usuario.hashear_clave()

        try:
            db.session.add(nuevo_usuario)
            db.session.commit()
            access_token = create_access_token(identity = request.json['email'], expires_delta = timedelta(days = 1))
            return {
                'message': f'El correo {request.json["email"]} ha sido registrado',
                'access_token': access_token 
            }

        except:
            return {'message':'Ha ocurrido un error'}, 500

'''
Recurso que lista todas los libros de los usuarios
'''
class RecursoLibro(Resource):
    def get(self):        
        parser = reqparse.RequestParser()
        parser.add_argument('limit', type = int, help='El limite no puede ser convertido')
        parser.add_argument('order')
        args = parser.parse_args()
        
        if args['order'] == 'desc':
            libros = Libro.query.order_by(db.desc(Libro.id)).limit(args['limit']).all()
        else:
            libros = Libro.query.order_by(db.asc(Libro.id)).limit(args['limit']).all()
        
        return libros_schema.dump(libros)    

'''
Recurso que administra el servicio de todas los de un usuario
'''
class RecursoLibros(Resource):
    @jwt_required()
    def get(self):        
        email = get_jwt_identity()        
        libros = Libro.query.filter_by(usuario_libro = email).order_by(db.desc(Libro.id)).all()
        return libros_schema.dump(libros)    
    
    @jwt_required()
    def post(self):
        email = get_jwt_identity()
        
        nueva_libro = Libro(
            titulo = request.json['titulo'],
            autor = request.json['autor'],
            categoria = request.json['categoria'],
            descripcion = request.json['descripcion'],
            idioma = request.json['idioma'],
            fecha_publicacion = request.json['fecha_publicacion'],
            revision = request.json['revision'],          
            usuario_libro = email
            )
        db.session.add(nueva_libro)
        db.session.commit()
        
        return libro_schema.dump(nueva_libro)

'''
Recurso que administra el servicio de una publicación (Detail)
'''
class RecursoMiLibro(Resource):
    @jwt_required()
    def get(self, id_libro):
        email = get_jwt_identity()
        libro = Libro.query.get_or_404(id_libro)

        if libro.usuario_libro != email:
            return {'message':'No tiene acceso a esta publicación'}, 401
        else:
            return libro_schema.dump(libro)

    @jwt_required()
    def put(self, id_libro):
        email = get_jwt_identity()
        libro = Libro.query.get_or_404(id_libro)        
        
        if libro.usuario_libro != email:
            return {'message':'No tiene acceso a este concurso'}, 401

        if 'titulo' in request.json:
            libro.titulo = request.json['titulo']
        
        if 'autor' in request.json:
            libro.autor = request.json['autor']

        if 'categoria' in request.json:
            libro.categoria = request.json['categoria']

        if 'descripcion' in request.json:
            libro.descripcion = request.json['descripcion']

        if 'idioma' in request.json:
            libro.idioma = request.json['idioma']

        if 'descripcion' in request.json:
            libro.descripcion = request.json['descripcion']

        if 'fecha_publicacion' in request.json:
            libro.fecha_publicacion = request.json['fecha_publicacion']

        if 'revision' in request.json:
            libro.revision = request.json['revision']

        db.session.commit()
        return libro_schema.dump(libro)

    @jwt_required()
    def delete(self, id_libro):
        email = get_jwt_identity()
        libro = Libro.query.get_or_404(id_libro)
        
        if libro.usuario_libro != email:
            return {'message':'No tiene acceso a esta publicación'}, 401
        
        db.session.delete(libro)
        db.session.commit()        
        return '', 204
        